from django import forms


class SertForm(forms.Form):
    name = forms.CharField(label='Nama: ', max_length=100, required=True, widget=forms.HiddenInput)
    no_sert = forms.CharField(label='Nomor Sertifikat: ', max_length=12, required=True, widget=forms.HiddenInput)
    status = forms.CharField(label='Status Tahapan: ', max_length=12, required=True, widget=forms.HiddenInput)
