from django.http.response import HttpResponseRedirect
from django.shortcuts import render

from django.db import connection
from collections import namedtuple

from kartu_vaksin.form_kv import SertForm

# from io import BytesIO
# from django.http import HttpResponse
# from django.template.loader import get_template
#
# from xhtml2pdf import pisa
#
#
# def render_to_pdf(template_src, context_dict={}):
#     template = get_template(template_src)
#     html = template.render(context_dict)
#     result = BytesIO()
#     pdf = pisa.pisaDocument(BytesIO(html.encode("ISO-8859-1")), result)
#     if not pdf.err:
#         return HttpResponse(result.getvalue(), content_type='application/pdf')
#     return None


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


def new_cursor():
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")
    return cursor


# Create your views here.
# def create(request):
#     return render(request, )

def read_user(request):  # Check Current User
    cursor = new_cursor()

    email = "Session"

    cursor.execute(f"SELECT * FROM kartu_vaksin WHERE email = '{email}';")

    data = namedtuplefetchall(cursor)
    cursor.close()
    return render(request, 'readKV.html', {'data': data})


def detail(request, kode, tiket):  # Read ID
    cursor = new_cursor()
    cursor.execute(f"SELECT * FROM kartu_vaksin WHERE no_sertifikat = '{kode}' AND no_tiket = '{tiket}';")
    kartu = namedtuplefetchall(cursor)

    email = kartu[0].__getattribute__('email')
    cursor.execute(f"SELECT nama_lengkap FROM warga WHERE email = '{email}'")
    nama = namedtuplefetchall(cursor)

    kv_form = SertForm()
    kv_form.name.initial(nama[0])
    kv_form.no_sert.initial(kartu.__getattribute__('no_sertifikat'))
    kv_form.status.initial(kartu.__getattribute__('status_tahapan'))

    cursor.close()

    if request.method == 'POST':
        pass
    return render(request, 'detailKV.html', {'sertform': kv_form})
