from django.urls import path
from . import views

app_name = 'kartu_vaksin'

urlpatterns = [
    path('', views.read_user, name='kv-read'),
    path('detail-sertifikat/<id>?<tiket>', views.detail, name='kv-detail')
]
