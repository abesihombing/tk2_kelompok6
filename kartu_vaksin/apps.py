from django.apps import AppConfig


class KartuVaksinConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'kartu_vaksin'
