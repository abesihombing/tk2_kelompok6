from django import forms


class AccountForm(forms.Form):
    email = forms.CharField(label='Email :', max_length=100, required=True)
    password = forms.CharField(label='Password :', max_length=100, required=True)
    phone = forms.CharField(label='No. HP :', max_length=12, required=True)


class FormData(forms.Form):
    status = forms.CharField(label='', initial="Belum Terverifikasi", required=True)


class ClientForm(forms.Form):
    id = forms.CharField(label='NIK :', max_length=100, required=True)
    name = forms.CharField(label='Nama Panjang:', max_length=100, required=True)
    gender = forms.ChoiceField(choices=(('M', 'Male'), ('F', 'Female')), label='Jenis Kelamin :', required=True)


class AddressForm(forms.Form):
    no = forms.CharField(label='Nomor Bangunan:', required=True)
    road = forms.CharField(label='Nama Jalan:', required=True)
    kel = forms.CharField(label='Kelurahan:', required=True)
    kec = forms.CharField(label='Kecamatan:', required=True)
    kab = forms.CharField(label='Kabupaten / Kota Administrasi:', required=True)


class AffiliateForm(forms.Form):
    def __init__(self, data, *args, **kwargs):
        super(AffiliateForm, self).__init__(*args, **kwargs)
        self.fields['affiliation'].choices = data

    affiliation = forms.ChoiceField(label='Instansi:', required=True, choices=())


class AdminForm(forms.Form):
    id_admin = forms.CharField(label='Nomor Petugas :', required=True)


class PersonnelForm(forms.Form):
    full_name = forms.CharField(label='Nama Lengkap: ', required=True)
    option = forms.CharField(label='', required=False, widget=forms.HiddenInput)
    STR = forms.CharField(label='Nomor Surat Tanda Registrasi: ', required=False)
    personnel_type = forms.CharField(label='Tipe Petugas:', required=False)
