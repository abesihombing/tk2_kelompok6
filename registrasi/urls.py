from django.urls import path
from . import views

app_name = 'registrasi'

urlpatterns = [
    path('', views.registration_page, name='register'),
    path('form/<int:type>', views.register_registration, name='register-form'),
]
