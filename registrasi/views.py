from django.shortcuts import render
from django.http.response import HttpResponseRedirect

from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def new_cursor():
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")
    return cursor

# Create your views here.
def registration_page(request):
    return render(request, 'registerAccount.html')


def register_registration(request, type):
    cursor = new_cursor()

    cursor.execute('SELECT * FROM instansi;')
    instansi = namedtuplefetchall(cursor)

    response = {'instansi': instansi}
    if int(type) == 0:
        # Registrasi Warga
        if request.method == 'POST':
            try:
                cursor.execute(f"INSERT INTO PENGGUNA VALUES ('{request.POST['email']}','{request.POST['phone']}','{request.POST['password']}','belum terverifikasi');")
                cursor.execute(f"INSERT INTO WARGA VALUES ('{request.POST['email']}','{request.POST['nik']}','{request.POST['nama']}','{request.POST['kelamin']}','{request.POST['bangunan']}','{request.POST['jalan']}','{request.POST['kel']}','{request.POST['kec']}','{request.POST['kab']}',{request.POST['instansi']});")
                cursor.close()
                return HttpResponseRedirect('../../../')
            except Exception as e:
                # response['message'] = e;
                response['message'] = 'Password setidaknya harus mengandung 1 huruf kapital dan 1 angka'
                print(f"Failed to insert record into admin_satgas table {e}")

        return render(request, 'registerWarga.html', response)

    elif int(type) == 1:
        # Registrasi Panitia Penyelenggara
        if request.method == 'POST':
            try:
                isNakes = (request.POST['radios'])
                cursor.execute(f"INSERT INTO PENGGUNA VALUES ('{request.POST['email']}','{request.POST['phone']}','{request.POST['password']}','belum terverifikasi');")
                cursor.execute(f"INSERT INTO PANITIA_PENYELENGGARA VALUES ('{request.POST['email']}', '{request.POST['nama']}');")
                if isNakes == "Ya":
                    cursor.execute(f"INSERT INTO NAKES VALUES('{request.POST['email']}', '{request.POST['nostr']}', '{request.POST['typePetugas']}')")
                cursor.execute(f"INSERT INTO WARGA VALUES ('{request.POST['email']}','{request.POST['nik']}','{request.POST['nama']}','{request.POST['kelamin']}','{request.POST['bangunan']}','{request.POST['jalan']}','{request.POST['kel']}','{request.POST['kec']}','{request.POST['kab']}',{request.POST['instansi']});")
                cursor.close()
                return HttpResponseRedirect('../../../')
            except Exception as e:
                # response['message'] = e;
                response['message'] = 'Password setidaknya harus mengandung 1 huruf kapital dan 1 angka'
                print(f"Failed to insert record into admin_satgas table {e}")

        return render(request, 'registerPanitia.html', response)

    else:
        # Registrasi Admin
        if request.method == 'POST':
            try:
                # print(f"INSERT INTO PENGGUNA VALUES ('{request.POST['email']}',{request.POST['phone']},'{request.POST['password']}','belum terverifikasi');")
                # print(f"INSERT INTO ADMIN_SATGAS (id_pegawai, email) VALUES ('{request.POST['id']}','{request.POST['email']}');")
                # print(f"INSERT INTO WARGA VALUES ('{request.POST['email']}',{request.POST['nik']},'{request.POST['nama']}','{request.POST['kelamin']}',{request.POST['bangunan']},'{request.POST['jalan']}','{request.POST['kel']}','{request.POST['kec']}','{request.POST['kab']}',{request.POST['instansi']});")
                cursor.execute(f"INSERT INTO PENGGUNA VALUES ('{request.POST['email']}','{request.POST['phone']}','{request.POST['password']}','belum terverifikasi');")
                cursor.execute(f"INSERT INTO ADMIN_SATGAS (id_pegawai, email) VALUES ('{request.POST['id']}','{request.POST['email']}');")
                cursor.execute(f"INSERT INTO WARGA VALUES ('{request.POST['email']}','{request.POST['nik']}','{request.POST['nama']}','{request.POST['kelamin']}','{request.POST['bangunan']}','{request.POST['jalan']}','{request.POST['kel']}','{request.POST['kec']}','{request.POST['kab']}',{request.POST['instansi']});")
                cursor.close()
                return HttpResponseRedirect('../../../')
            except Exception as e:
                # response['message'] = e;
                response['message'] = 'Password setidaknya harus mengandung 1 huruf kapital dan 1 angka'
                print(f"Failed to insert record into admin_satgas table {e}")


        cursor.close()
        return render(request, 'registerAdmin.html', response)
