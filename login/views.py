from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db import connection
from collections import namedtuple
from django.contrib import messages


# Create your views here.
def login(request): #Check Current User
    if 'email' in request.session:
        response = {}
        response['email'] = request.session['email']
        return render(request, 'dashboard.html', response)
    if request.method == "POST":
        email = request.POST.get("email", None)
        password = request.POST.get("password", None)
        valResult = checkUser("pengguna", email, password)
        if valResult:
            cursor = connection.cursor()
            cursor.execute("set search_path to sivax")
            cursor.execute("update pengguna values set status_verifikasi = 'sudah terverifikasi' where email = '{}'".format(email))
            request.session['email'] = email
            request.session['warga'] = True
            listKemungkinanRoleUser = ["admin_satgas", "nakes", "panitia_penyelenggara", "warga"]
            for role in listKemungkinanRoleUser:
                if(checkUser(role, email, None)):
                    request.session['role'] = role
                    request.session[role] = True
                    if (role == "nakes"):
                        request.session["panitia_penyelenggara"] = True
                    break
            response = {}
            response['email'] = email
            cursor.close()
            return render(request, 'dashboard.html', response)
        else:
            messages.error(request, 'Email/Password Anda tidak terdaftar dalam sistem')

    return render(request, 'login.html')

def logout(request):
    if 'email' not in request.session:
        return HttpResponseRedirect('/')
    request.session.flush()
    return HttpResponseRedirect('/')


def checkUser(tableName, email, password):
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    hasil=''
    if tableName=="pengguna":
        cursor.execute("select * from {} where email='{}' and password='{}'".format(tableName, email, password))
        hasil = namedtuplefetchall(cursor)
    else:
        cursor.execute("select * from {} where email= '{}'".format(tableName, email))
        hasil = namedtuplefetchall(cursor)
    if hasil:
        return True
    return False

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
