from django.apps import AppConfig


class StatusTiketConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'status_tiket'
