from django.urls import path
from . import views

app_name = 'status_tiket'

urlpatterns = [
    path('', views.read_status_tiket, name='r-status-tiket'),
    path('create/', views.create_status_tiket, name='c-status-tiket'),
    path('delete/', views.delete_status_tiket, name='d-status-tiket'),
    path('update/', views.update_status_tiket, name='u-status-tiket')
]
