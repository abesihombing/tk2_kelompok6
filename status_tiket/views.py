from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.http.response import HttpResponse, HttpResponseRedirect

# Asumsi semuanya valid, tidak perlu handle validasi kalau user masukkin id/kode asal2an di URL
# Asumsi berlaku untuk seluruh bagian CRUD instansi, CRUD status_tiket, dan CR tiket

# Create your views here.
def read_status_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses status tiket")
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select * from status_tiket")
    hasil = namedtuplefetchall(cursor)
    response['status_tiket'] = hasil
    cursor.close()
    return render(request, 'readStatusTiket.html', response)

def create_status_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses status tiket")

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select kode from status_tiket order by kode asc")
    hasil = namedtuplefetchall(cursor)

    listKode = []
    for kode in hasil:
        listKode.append(int(kode.kode))
    listKode.sort()
    newPK = listKode[-1]+1
    response['kode_tiket'] = newPK
    
    if request.method == "POST":
        nama_status = request.POST.get("nama_status")
        cursor.execute("insert into status_tiket values ('" + str(newPK) + "', '"+nama_status+"')")
        cursor.close()
        return HttpResponseRedirect('../')
    cursor.close()
    return render(request, 'createStatusTiket.html', response)

# update view for details
def update_status_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses status tiket")

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select * from status_tiket where kode = '" + str(request.GET.get("id", None)) + "'")
    hasil = namedtuplefetchall(cursor)
    response['status_tiket'] = hasil[0]
    if request.method == "POST":
        cursor.execute("select * from tiket where kode_status = '{}'".format(hasil[0].kode)) 
        hasil2 = namedtuplefetchall(cursor)
        if len(hasil2)>0:
            cursor.close()
            return HttpResponse("Anda tidak dapat mengupdate status tiket ini karena ada yang sedang menggunakannya")

        nama_status = request.POST.get("nama_status")
        cursor.execute("update status_tiket set nama_status='" +nama_status+"' where kode='"+ hasil[0].kode + "'")
        cursor.close()
        return HttpResponseRedirect('../')
    cursor.close()
    return render(request, "updateStatusTiket.html", response)

def delete_status_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses status tiket")
        
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    pk = str(request.GET.get("id", None))

    cursor.execute("select * from status_tiket where kode = '" + pk + "'")
    hasil = namedtuplefetchall(cursor)
    response['status_tiket'] = hasil[0]

    if request.method == "POST":
        cursor.execute("select * from tiket where kode_status = '{}'".format(pk))
        hasil = namedtuplefetchall(cursor)
        if len(hasil)>0:
            return HttpResponse("Anda tidak dapat menghapus status tiket ini karena ada yang sedang menggunakannya")

        cursor.execute("delete from status_tiket where kode = '" + pk + "'")
        cursor.close()
        return HttpResponseRedirect('../')
    cursor.close()
    return render(request, "deleteStatusTiket.html", response)

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
