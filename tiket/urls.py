from django.urls import path
from . import views

app_name = 'tiket'

urlpatterns = [
    path('', views.read_jadwal, name='r-jadwal'),
    path('create/', views.create_tiket, name='c-tiket'),
    path('read-mytiket/', views.read_tiket, name='r-tiket'),
    path('detail-mytiket/', views.detail_tiket, name='det-tiket'),
    path('update-tiket/<id>', views.update, name='tiket-update')
]