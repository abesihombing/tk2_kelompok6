from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.db import connection
from collections import namedtuple
from django.contrib import messages


# Asumsi semuanya valid, tidak perlu handle validasi kalau user masukkin id/kode asal2an di URL
# Asumsi berlaku untuk seluruh bagian CRUD instansi, CRUD status_tiket, dan CR tiket

# Create your views here.
def read_jadwal(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute(
        "select I,kode, I.nama_instansi, P.tanggal_waktu, P.jumlah from penjadwalan P, instansi I where status='pengajuan disetujui' and P.kode_instansi = I.kode")
    hasil = namedtuplefetchall(cursor)

    newData = []
    for result in hasil:
        newData.append(
            {'kode': result.kode, 'nama_instansi': result.nama_instansi, 'tanggal_waktu': str(result.tanggal_waktu),
             'jumlah': result.jumlah})
    response['jadwal'] = newData
    cursor.close()
    return render(request, 'readJadwal.html', response)


def checkUser(tableName, email, password):
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    hasil = ''
    if tableName == "pengguna":
        cursor.execute("select * from {} where email='{}' and password='{}'".format(tableName, email, password))
        hasil = namedtuplefetchall(cursor)
    else:
        cursor.execute("select * from {} where email= '{}'".format(tableName, email))
        hasil = namedtuplefetchall(cursor)
    if hasil:
        return True
    return False


def create_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    response = {}
    kode = request.GET.get("kode", None)
    tanggal_waktu = request.GET.get("tanggal_waktu", None)
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("""select I.nama_instansi, P.tanggal_waktu, P.kategori_penerima, LV.nama, P.jumlah, I.kode
                    from instansi I, penjadwalan P, lokasi_vaksin LV
                    where P.kode_instansi = I.kode and LV.kode=P.kode_lokasi 
                    and P.kode_instansi = '{}' and P.tanggal_waktu = '{}' """.format(kode, tanggal_waktu))
    hasil = namedtuplefetchall(cursor)
    response['jadwal'] = hasil[0]
    response['tanggal_waktu'] = str(hasil[0].tanggal_waktu)

    cursor.execute("select no_tiket from tiket")
    hasil2 = namedtuplefetchall(cursor)
    listPk = []
    for key in hasil2:
        listPk.append(int(key.no_tiket[4:]))
    listPk.sort()
    newPk = 'tkt0' + str(listPk[-1] + 1)

    if request.method == "POST":
        if 'email' not in request.session:
            return HttpResponse('<a href="/" >Login</a> dulu yuk')
        try:
            cursor.execute("select kode from status_tiket where nama_status='terdaftar'")
            hasil3 = namedtuplefetchall(cursor)
            cursor.execute("insert into tiket values('{}', '{}','{}','{}','{}')".format(request.session['email'], newPk,
                                                                                        hasil[0].kode, hasil3[0].kode,
                                                                                        str(hasil[0].tanggal_waktu)))
            cursor.close()
            return HttpResponseRedirect("../")
        except Exception as e:
            index = str(e).find("CONTEXT")
            msg = str(e)[:index - 1]
            messages.error(request, msg)

    return render(request, 'createTiket.html', response)


def read_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    if request.session['role'] != 'nakes':
        cursor.execute("""select T.no_tiket, I.nama_instansi, T.tgl_waktu, S.nama_status
                        from tiket T, instansi I, status_tiket S
                        where T.kode_instansi = I.kode and T.kode_status=S.kode and T.email='{}'""".format(
            request.session['email']))

    else:
        cursor.execute("""select T.no_tiket, I.nama_instansi, T.tgl_waktu, S.nama_status
                        from tiket T, instansi I, status_tiket S
                        where T.kode_instansi = I.kode and T.kode_status=S.kode""")

    hasil = namedtuplefetchall(cursor)
    newData = []
    for result in hasil:
        newData.append(
            {'no_tiket': result.no_tiket, 'nama_instansi': result.nama_instansi, 'tgl_waktu': str(result.tgl_waktu),
             'nama_status': result.nama_status})
    response['tiket_saya'] = newData
    cursor.close()
    return render(request, 'readMyTiket.html', response)


def detail_tiket(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')

    kode = request.GET.get("no_tiket", None)
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("""select T.no_tiket, I.nama_instansi, T.tgl_waktu, S.nama_status, P.kategori_penerima, LV.nama, P.jumlah
                    from tiket T, instansi I, status_tiket S, penjadwalan P, lokasi_vaksin LV
                    where T.kode_instansi = I.kode and T.kode_status=S.kode and P.kode_instansi = I.kode 
                    and LV.kode=P.kode_lokasi and T.no_tiket = '{}'""".format(kode))
    hasil = namedtuplefetchall(cursor)
    hasil = hasil[0]
    response['hasil'] = hasil
    response['tgl_waktu'] = str(hasil.tgl_waktu)

    cursor.close()
    return render(request, 'detailMyTiket.html', response)


def update(request, id):
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")

    if request.method == 'POST':
        if 'email' not in request.session:
            return HttpResponse('<a href="/" >Login</a> dulu yuk')
        try:
            cursor.execute(f"UPDATE tiket SET kode_status = '{request.POST['statusTiket']}' WHERE no_tiket = '{id}';")
        except Exception as e:
            index = str(e).find("CONTEXT")
            msg = str(e)[:index - 1]
            messages.error(request, msg)

    cursor.execute("""select T.no_tiket, I.nama_instansi, T.tgl_waktu, S.nama_status, P.kategori_penerima, LV.nama, P.jumlah
                            from tiket T, instansi I, status_tiket S, penjadwalan P, lokasi_vaksin LV
                            where T.kode_instansi = I.kode and T.kode_status=S.kode and P.kode_instansi = I.kode 
                            and LV.kode=P.kode_lokasi and T.no_tiket = '{}'""".format(id))
    hasil = namedtuplefetchall(cursor)
    cursor.execute("SELECT * FROM status_tiket")
    statusTiket = namedtuplefetchall(cursor)

    cursor.close()

    try:
        hasil = hasil[0]
    except IndexError:
        return HttpResponseRedirect('no tiket found')

    return render(request, 'updateTiket.html', {'hasil': hasil, 'statusTiket': statusTiket})


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
