asgiref==3.4.1
coverage==5.5
dj-database-url==0.5.0
Django==3.2.7
djangorestframework==3.12.4
gunicorn==20.1.0
psycopg2-binary==2.9.2
python-dotenv==0.19.2
pytz==2021.1
selenium==3.141.0
sqlparse==0.4.2
urllib3==1.26.7
whitenoise==5.3.0
xhtml2pdf==0.2.5
