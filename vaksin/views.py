from django.shortcuts import render
from django.http.response import HttpResponseRedirect
import datetime
from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse

from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def new_cursor():
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")
    return cursor

def cek_login(request, role):
    if 'email' in request.session:
        if request.session['role'] in role:
            return True
        else:
            return False
    else:
        return render(request, 'cannot.html')

# Create your views here.
def create_vax(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses vaksin")

    cursor = new_cursor()
    cursor.execute("SELECT max(kode) FROM vaksin group by kode;")
    kode = namedtuplefetchall(cursor)
    newPk = "vcn0" + str(int(kode[0].max[-1])+2)

    if request.method == 'POST':
        cursor.execute(f"INSERT INTO VAKSIN VALUES ('{newPk}','{request.POST['namavax']}','{request.POST['namaprodusen']}','{request.POST['noedar']}','{request.POST['freqsuntik']}','{request.POST['stok']}');")
        cursor.close()
        return HttpResponseRedirect('../')
    
    return render(request, 'createvax.html')
    

def read_vax(request): #Read ID
    cursor = new_cursor()

    cursor.execute("SELECT * FROM vaksin;")
    vak = namedtuplefetchall(cursor)

    vak_new = []
    for i in vak:
        sem = {
            'kode':i.kode,
            'nama':i.nama,
            'nama_produsen':i.nama_produsen,
            'no_edar':i.no_edar,
            'freq_suntik':i.freq_suntik,
            'stok':i.stok,
        }
        cursor.execute(f"SELECT * FROM distribusi where kode_vaksin='{i.kode}';")
        dis = namedtuplefetchall(cursor)
        if len(dis)>0:
            sem['dis'] = True
        else:
            sem['dis'] = False

        vak_new.append(sem)

    response = {'vaksin': vak_new}

    cursor.close()
    return render(request, 'readvax.html', response)

def update_vax(request, kode):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute(f"SELECT * FROM vaksin WHERE kode = '{kode}';")
    vak = namedtuplefetchall(cursor)[0]
    cursor.execute(f"SELECT * FROM update_stok where kode_vaksin='{kode}';")
    update = namedtuplefetchall(cursor)

    upd_new = []
    for i in update:
        sem = {
            'email_pegawai':i.email_pegawai,
            'tgl_waktu':str(i.tgl_waktu),
            'jumlah_update':i.jumlah_update,
        }
        upd_new.append(sem)


    response = {
        'kode':kode,
        'nama':vak.nama,
        'update':upd_new,
    }

    cursor.close()
    return render(request, 'updatevax.html', response)

def update_stok(request, kode):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute(f"SELECT * FROM vaksin WHERE kode = '{kode}';")
    vak = namedtuplefetchall(cursor)[0]

    response = {'vaksin': vak}

    if request.method == 'POST':
        try:
            tgl = str(datetime.datetime.now())[:19]
            cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {request.POST['jumlah_update']},'{vak.kode}');")
            cursor.close()
            return HttpResponseRedirect('../')
        except Exception as e:
            response['message'] = 'Jumlah vaksin melebihi stok vaksin'
            
    # print(request.POST['jumlah_update'])
    # print(datetime.datetime.now())

    cursor.close()
    return render(request, 'updatestok.html', response)

def delete_vax(request, kode):
    cursor = new_cursor()
    cursor.execute(f"DELETE FROM vaksin WHERE kode = '{kode}';")
    return HttpResponseRedirect('../../')

