from django.urls import path
from . import views

app_name = 'vaksin'

urlpatterns = [
    path('', views.read_vax),
    path('createvax/', views.create_vax),
    path('updatevax/<str:kode>/',views.update_vax),
    path('deletevax/<str:kode>/',views.delete_vax),
    path('updatevax/<str:kode>/upd_stok/',views.update_stok),
]
