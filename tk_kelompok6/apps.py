from django.apps import AppConfig


class TkKelompok6Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tk_kelompok6'
