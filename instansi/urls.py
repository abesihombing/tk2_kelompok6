from django.urls import path
from . import views

app_name = 'instansi'

urlpatterns = [
    path('', views.read_instansi, name='r-instansi'),
    path('create/', views.create_instansi, name='c-instansi'),
    path('delete/', views.delete_instansi, name='d-instansi'),
    path('update/', views.update_instansi, name='u-instansi'),
    path('detail/', views.detail_instansi, name='det-instansi'),
    path('create/ajax/', views.create_instansi_ajax, name='a-c-instansi'),
    path('update/ajax/', views.update_instansi_ajax, name='a-u-instansi')
]
