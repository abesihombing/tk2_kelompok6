from django.apps import AppConfig


class InstansiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'instansi'
