from django.http.response import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import render
from django.db import connection
from collections import namedtuple

# Asumsi semuanya valid, tidak perlu handle validasi kalau user masukkin id/kode asal2an di URL
# Asumsi berlaku untuk seluruh bagian CRUD instansi, CRUD status_tiket, dan CR tiket

# Create your views here.
def read_instansi(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses instansi")

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select * from instansi")
    hasil = namedtuplefetchall(cursor)
    response['instansi'] = hasil
    cursor.close()
    return render(request, 'readInstansi.html', response)

def detail_instansi(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses instansi")

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select * from instansi where kode = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    response['instansi'] = hasil[0]

    cursor.execute("select * from instansi_faskes where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    if(len(hasil)>0):
        response['instansi_faskes'] = hasil[0]

    cursor.execute("select * from instansi_non_faskes where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    if(len(hasil)>0):
        response['instansi_non_faskes'] = hasil[0]

    cursor.execute("select * from instansi_telepon where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    response['instansi_telepon'] = hasil[0]

    cursor.close()
    return render(request, 'detailInstansi.html', response)

def create_instansi(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses instansi")

    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")

    # Order by kode akan memberi bug ketika data > 999 karean 'D1000' < 'D999'
    # Oleh karena itu dilakukan sorting manual menggunakan python
    cursor.execute("select kode from instansi")
    hasil = namedtuplefetchall(cursor)

    listKode = []
    for kode in hasil:
        listKode.append(int(kode.kode[1:]))
    listKode.sort()
    kodeAngka = listKode[-1]
    newPKAngka = kodeAngka+1
    newPK = "D" + str(newPKAngka).zfill(3)
    response = {'kode_instansi': newPK}

    cursor.close()
    return render(request, 'createInstansi.html', response)

def create_instansi_ajax(request):
    if request.method=="POST":
        if 'email' not in request.session:
            return HttpResponse('<a href="/" >Login</a> dulu yuk')
        if (request.session['role'] != 'admin_satgas'):
            return HttpResponse("Hanya admin yang boleh mengakses instansi")

        cursor = connection.cursor()
        cursor.execute("set search_path to sivax")
        
        kode_instansi = request.POST.get("dataInstansi[kodeInstansi]", None)
        nama_instansi = request.POST.get("dataInstansi[namaInstansi]", None)
        cursor.execute("insert into instansi values ('{}', '{}')".format(kode_instansi, nama_instansi))

        # Asumsi nomor telepon hanya ada satu karena tidak ada keterangan cara supaya input banyak di soal TK
        nomor_telepon = request.POST.get("dataInstansi[nomorTelepon]", None)
        cursor.execute("insert into instansi_telepon values ('{}', '{}')".format(kode_instansi, nomor_telepon))
        

        kategori_instansi = request.POST.get("dataInstansi[kategoriInstansi]", None)

        if kategori_instansi == 'Faskes':
            tipe_faskes = request.POST.get("dataInstansi[tipeFaskes]", None)
            status_kepemilikan = request.POST.get("dataInstansi[statusKepemilikan]", None)
            cursor.execute("insert into instansi_faskes values ('{}', '{}', '{}')".format(kode_instansi, tipe_faskes, status_kepemilikan))
            cursor.close()
            return JsonResponse({'status': 'success'})
        else:
            tipe_non_faskes = request.POST.get("dataInstansi[tipeNonFaskes]", None)
            cursor.execute("insert into instansi_non_faskes values ('{}', '{}')".format(kode_instansi, tipe_non_faskes))
            cursor.close()
            return JsonResponse({'status': 'success'})
        
# update view for details
def update_instansi(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses instansi")

    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")
    cursor.execute("select * from instansi where kode = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    response['instansi'] = hasil[0]

    cursor.execute("select * from instansi_faskes where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    if(len(hasil)>0):
        response['instansi_faskes'] = hasil[0]

    cursor.execute("select * from instansi_non_faskes where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    if(len(hasil)>0):
        response['instansi_non_faskes'] = hasil[0]

    cursor.execute("select * from instansi_telepon where kode_instansi = '" + request.GET.get("kode", None) + "'")
    hasil = namedtuplefetchall(cursor)
    response['instansi_telepon'] = hasil[0]

    cursor.close()
    return render(request, "updateInstansi.html", response)

def update_instansi_ajax(request):
    if request.method=="POST":
        if 'email' not in request.session:
            return HttpResponse('<a href="/" >Login</a> dulu yuk')
        if (request.session['role'] != 'admin_satgas'):
            return HttpResponse("Hanya admin yang boleh mengakses instansi")

        cursor = connection.cursor()
        cursor.execute("set search_path to sivax")

        kode_instansi = request.POST.get("dataInstansi[kodeInstansi]", None)
        cursor.execute("select * from penjadwalan where kode_instansi = '{}'".format(kode_instansi))
        hasil = namedtuplefetchall(cursor)
        if len(hasil)>0:
            cursor.close()
            return JsonResponse({"status": "error", "message": "Anda tidak dapat mengupdate instansi ini karena sedang digunakan dalam penjadwalan"})
        
        old_kategori = request.POST.get("oldKategori", None)
        
        nama_instansi = request.POST.get("dataInstansi[namaInstansi]", None)
        cursor.execute("update instansi values set nama_instansi = '" + nama_instansi + "' where kode = '" + kode_instansi + "'")

        # Asumsi nomor telepon hanya 1 karena tidak diberitahu cara agar input bisa banyak
        nomor_telepon = request.POST.get("dataInstansi[nomorTelepon]", None)
        cursor.execute("update instansi_telepon values set no_telp = '{}' where kode_instansi = '{}'".format(nomor_telepon, kode_instansi))
        

        kategori_instansi = request.POST.get("dataInstansi[kategoriInstansi]", None)

        # Case 1, kategori sama
        if (kategori_instansi == old_kategori):
            if (kategori_instansi == 'Faskes'):
                tipe_faskes = request.POST.get("dataInstansi[tipeFaskes]", None)
                status_kepemilikan = request.POST.get("dataInstansi[statusKepemilikan]", None)
                cursor.execute("update instansi_faskes values set tipe = '{}', statuskepemilikan = '{}'".format(tipe_faskes, status_kepemilikan))
                cursor.close()
                return JsonResponse({'status': 'success'})
            else:
                tipe_non_faskes = request.POST.get("dataInstansi[tipeNonFaskes]", None)
                cursor.execute("update instansi_non_faskes values set kategori = '{}' where kode_instansi = '{}'".format(tipe_non_faskes, kode_instansi))
                cursor.close()
                return JsonResponse({'status': 'success'})

        # Case 2, ganti kategori
        else:
            if kategori_instansi == 'Faskes':
                cursor.execute("delete from instansi_non_faskes where kode_instansi = '{}'".format(kode_instansi))
                tipe_faskes = request.POST.get("dataInstansi[tipeFaskes]", None)
                status_kepemilikan = request.POST.get("dataInstansi[statusKepemilikan]", None)
                cursor.execute("insert into instansi_faskes values ('{}', '{}', '{}')".format(kode_instansi, tipe_faskes, status_kepemilikan))
                cursor.close()
                return JsonResponse({'status': 'success'})
            else:
                cursor.execute("delete from instansi_faskes where kode_instansi = '{}'".format(kode_instansi))
                tipe_non_faskes = request.POST.get("dataInstansi[tipeNonFaskes]", None)
                cursor.execute("insert into instansi_non_faskes values ('{}', '{}')".format(kode_instansi, tipe_non_faskes))
                cursor.close()
                return JsonResponse({'status': 'success'})

def delete_instansi(request):
    if 'email' not in request.session:
        return HttpResponse('<a href="/" >Login</a> dulu yuk')
    if (request.session['role'] != 'admin_satgas'):
        return HttpResponse("Hanya admin yang boleh mengakses instansi")
    response = {}
    cursor = connection.cursor()
    cursor.execute("set search_path to sivax")

    kode_instansi = request.GET.get("kode", None)
    cursor.execute("select * from instansi where kode = '" + kode_instansi + "'")
    hasil = namedtuplefetchall(cursor)
    response['instansi'] = hasil[0]

    if(request.method == "POST"):
        cursor.execute("select * from penjadwalan where kode_instansi = '{}'".format(kode_instansi))
        hasil2 = namedtuplefetchall(cursor)
        if len(hasil2)>0:
            cursor.close()
            return HttpResponse("Anda tidak dapat menghapus instansi ini karena ada yang sedang menggunakannya")

        cursor.execute("delete from instansi where kode = '{}'".format(kode_instansi))
        cursor.close()
        return HttpResponseRedirect('../')
    return render(request, "deleteInstansi.html", response)

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


    