# Create your views here.
from django.http import response
from django.http.request import RAISE_ERROR
from django.shortcuts import render
from django.http.response import HttpResponseRedirect

from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def cek_login(request, role):
    if 'email' in request.session:
        if request.session['role'] in role:
            return True
        else:
            return False
    else:
        return render(request, 'cannot.html')


# Create your views here.
def read_penjadwalan(request):
    if (not cek_login(request, ["admin_satgas", "nakes", "panitia_penyelenggara"])):
        return render(request, 'cannot.html')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")

    # print(request.session['role'])

    if request.session['role'] in ["admin_satgas"]:
        cursor.execute("SELECT * FROM penjadwalan JOIN instansi i on i.kode = penjadwalan.kode_instansi;")
    else:
        # print()
        cursor.execute(f"SELECT * FROM penjadwalan JOIN instansi i on i.kode = penjadwalan.kode_instansi WHERE i.kode in (SELECT kode_instansi FROM panitia_pada_instansi WHERE email_panitia = '{request.session['email']}');")

   # cursor.execute("SELECT * FROM penjadwalan JOIN instansi i on i.kode = penjadwalan.kode_instansi;")

    penjadwalan_data = namedtuplefetchall(cursor)
    cursor.close()

    penjadwalan_data_new = []
    for i in penjadwalan_data:
        setujui = False
        if i.status == 'pengajuan disetujui':
            setujui = True
        distri = False
        if i.kode_distribusi != None:
            distri = True
        kirim = False
        if i.status == 'pengajuan dikirim':
            kirim = True
        penjadwalan_data_new.append({'nama_instansi':i.nama_instansi, 'tanggal_waktu':str(i.tanggal_waktu), 'jumlah':i.jumlah, 'status':i.status, 'kode_distribusi':i.kode_distribusi, 'kode':i.kode, 'setujui':setujui, 'distribusi':distri, 'kirim':kirim})

    response = {'penjadwalan_data' : penjadwalan_data_new}
    return render(request, 'readPenjadwalan.html', response)



def create_penjadwalan(request):
    if (not cek_login(request, ["nakes", "panitia_penyelenggara"])):
        return render(request, 'cannot.html')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")

    if request.method == 'POST':
        # print(request.POST)
        # print(request.POST['tanggal'])
        tanggal = request.POST['tanggal'][:10] + ' ' + request.POST['tanggal'][11:]
        # print(tanggal)
        cursor.execute("""INSERT INTO PENJADWALAN VALUES
            ('{}',
            '{}',
            {},
            '{}',
            'pengajuan dikirim',
            NULL,
            '{}',
            NULL,
            NULL);""".format(request.POST['instansi'], tanggal, request.POST['kuota'], request.POST['penerima'], request.POST['lokasi']))
        cursor.close()
        return HttpResponseRedirect('../')

    cursor.execute(f"SELECT * FROM instansi where kode in (SELECT kode_instansi FROM panitia_pada_instansi WHERE email_panitia = '{request.session['email']}');")
    instansi_data = namedtuplefetchall(cursor)
    cursor.execute("SELECT kode, nama FROM lokasi_vaksin;")
    lokasi_data = namedtuplefetchall(cursor)
    cursor.close()

    response = {'instansi_data' : instansi_data, 'lokasi_data' : lokasi_data}

    # if request.method == 'POST':
    #     print(request.POST)

    
    return render(request, 'createPenjadwalan.html', response)

def update_penjadwalan(request, id, tgl):
    if (not cek_login(request, ["nakes", "panitia_penyelenggara"])):
        return render(request, 'cannot.html')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")

    cursor.execute("SELECT * FROM penjadwalan WHERE kode_instansi = '" + id + "' AND tanggal_waktu = '" + tgl + "';")
    pjd = namedtuplefetchall(cursor)[0]

    if request.method == 'POST':
        # print(request.POST)
        # print(request.POST['tanggal'])
        tanggal = request.POST['tanggal'][:10] + ' ' + request.POST['tanggal'][11:]
        # print(tanggal)
        cursor.execute("UPDATE penjadwalan SET tanggal_waktu = '"+tanggal+"', kode_instansi = '"+request.POST['instansi']+"', jumlah = "+request.POST['kuota']+", kategori_penerima = '"+request.POST['penerima']+"', kode_lokasi = '"+request.POST['lokasi']+"', status = 'pengajuan dikirim' WHERE kode_instansi = '"+pjd.kode_instansi+"' AND tanggal_waktu = '"+str(pjd.tanggal_waktu)+"';")
        cursor.close()
        return HttpResponseRedirect('../../../')

    cursor.execute(f"SELECT * FROM instansi where kode in (SELECT kode_instansi FROM panitia_pada_instansi WHERE email_panitia = '{request.session['email']}');")
    instansi_data = namedtuplefetchall(cursor)
    cursor.execute("SELECT kode, nama FROM lokasi_vaksin;")
    lokasi_data = namedtuplefetchall(cursor)

    instansi_data_new = []
    for i in instansi_data:
        sem = {'kode':i.kode, 'nama_instansi':i.nama_instansi}
        if i.kode == pjd.kode_instansi:
            sem['select'] = True
        instansi_data_new.append(sem)
    
    lokasi_data_new = []
    for i in lokasi_data:
        sem = {'kode':i.kode, 'nama':i.nama}
        if i.kode == pjd.kode_lokasi:
            sem['select'] = True
        lokasi_data_new.append(sem)

    tanggal = str(pjd.tanggal_waktu)
    tanggal = tanggal[:10] + 'T' + tanggal[11:]

    response = { 'tanggal_waktu':tanggal, 'kuota':pjd.jumlah, 'status': pjd.status, 'instansi_data':instansi_data_new, 'lokasi_data':lokasi_data_new}
    
    if (pjd.kategori_penerima == 'umum'):
        response['umum'] = True
    else:
        response['internal'] = True

    cursor.close()
    return render(request, "updatePenjadwalan.html", response)

def detail_penjadwalan(request, id, tgl):
    if (not cek_login(request, ["admin_satgas", "nakes", "panitia_penyelenggara"])):
        return render(request, 'cannot.html')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")

    cursor.execute("SELECT * FROM penjadwalan WHERE kode_instansi = '" + id + "' AND tanggal_waktu = '" + tgl + "';")
    pjd = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM instansi WHERE kode = '" + pjd.kode_instansi + "';")
    ins = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM lokasi_vaksin WHERE kode = '" + pjd.kode_lokasi + "';")
    lok = namedtuplefetchall(cursor)[0]
    
    
    response = {'nama_instansi':ins.nama_instansi, 'tanggal_waktu':str(pjd.tanggal_waktu), 'kuota':pjd.jumlah, 'penerima':pjd.kategori_penerima, 'lokasi_vak':lok.nama,'status': pjd.status}
    if pjd.jumlah_nakes != None:
        response['jumlah_nakes'] = pjd.jumlah_nakes
    
    if pjd.email_admin != None:
        response['email_admin'] = pjd.email_admin

    if pjd.status == 'pengajuan disetujui':
        response['setuju'] = True

    
    if pjd.kode_distribusi != None:
        cursor.execute("SELECT * FROM distribusi WHERE kode = '" + pjd.kode_distribusi + "';")
        dis = namedtuplefetchall(cursor)[0]
        cursor.execute("SELECT * FROM vaksin WHERE kode = '" + dis.kode_vaksin + "';")
        vak = namedtuplefetchall(cursor)[0]
        response['kode_dis'] = dis.kode
        response['tgl_dis'] = dis.tanggal
        response['biaya'] = dis.biaya
        response['jenis_vaksin'] = vak.nama
        response['jumlah_dis'] = dis.jumlah_vaksin

    cursor.close()
    return render(request, 'detailPenjadwalan.html', response)

def verifikasi_penjadwalan(request, id, tgl):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")

    cursor.execute("SELECT * FROM penjadwalan WHERE kode_instansi = '" + id + "' AND tanggal_waktu = '" + tgl + "';")
    pjd = namedtuplefetchall(cursor)[0]
    cursor.execute(f"SELECT * FROM instansi_non_faskes WHERE kode_instansi = '{pjd.kode_instansi}';")
    non_faskes = len(namedtuplefetchall(cursor)) > 0


    if request.method == 'POST':
        # print(request.POST)
        # print(request.POST['tanggal'])
        tanggal = request.POST['tanggal'][:10] + ' ' + request.POST['tanggal'][11:]
        # print(tanggal)

        if request.POST.get('setujui'):
            status = 'pengajuan disetujui'
        else:
            status = 'pengajuan ditolak'

        if non_faskes:
            cursor.execute("UPDATE penjadwalan SET email_admin='"+request.session['email'] +"', tanggal_waktu = '"+tanggal+"', jumlah = "+request.POST['kuota']+", jumlah_nakes = "+ request.POST['jumlah_nakes']+", status = '"+ status+"' WHERE kode_instansi = '"+pjd.kode_instansi+"' AND tanggal_waktu = '"+str(pjd.tanggal_waktu)+"';")
        else:
            cursor.execute("UPDATE penjadwalan SET email_admin='"+request.session['email'] +"', tanggal_waktu = '"+tanggal+"', jumlah = "+request.POST['kuota']+", status = '"+ status+"' WHERE kode_instansi = '"+pjd.kode_instansi+"' AND tanggal_waktu = '"+str(pjd.tanggal_waktu)+"';")

        cursor.close()
        return HttpResponseRedirect('../../../')

    cursor.execute("SELECT * FROM instansi;")
    instansi_data = namedtuplefetchall(cursor)
    cursor.execute("SELECT kode, nama FROM lokasi_vaksin;")
    lokasi_data = namedtuplefetchall(cursor)

    instansi_data_new = []
    for i in instansi_data:
        sem = {'kode':i.kode, 'nama_instansi':i.nama_instansi}
        if i.kode == pjd.kode_instansi:
            sem['select'] = True
        instansi_data_new.append(sem)
    
    lokasi_data_new = []
    for i in lokasi_data:
        sem = {'kode':i.kode, 'nama':i.nama}
        if i.kode == pjd.kode_lokasi:
            sem['select'] = True
        lokasi_data_new.append(sem)

    tanggal = str(pjd.tanggal_waktu)
    tanggal = tanggal[:10] + 'T' + tanggal[11:]

    response = { 'non_faskes':non_faskes, 'tanggal_waktu':tanggal, 'kuota':pjd.jumlah, 'status': pjd.status, 'instansi_data':instansi_data_new, 'lokasi_data':lokasi_data_new}

    if (pjd.kategori_penerima == 'umum'):
        response['umum'] = True
    else:
        response['internal'] = True

    cursor.close()
    return render(request, "verifikasiPenjadwalan.html", response)

    