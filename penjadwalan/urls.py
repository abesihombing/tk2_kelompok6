from django.urls import path
from . import views

app_name = 'penjadwalan'

urlpatterns = [
    path('', views.read_penjadwalan, name='r-penjadwalan'),
    path('create/', views.create_penjadwalan, name='c-penjadwalan'),
    path('update/<str:id>/<str:tgl>/', views.update_penjadwalan, name='u-penjadwalan'),
    path('detail/<str:id>/<str:tgl>/', views.detail_penjadwalan, name='det-penjadwalan'),
    path('verifikasi/<str:id>/<str:tgl>/', views.verifikasi_penjadwalan, name='ver-penjadwalan'),
]
