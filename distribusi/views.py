
# Create your views here.
from django.http.response import HttpResponseRedirect
from django.shortcuts import render
import datetime

from django.db import connection
from collections import namedtuple

def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def new_cursor():
    cursor = connection.cursor()
    cursor.execute("SET SEARCH_PATH TO SIVAX;")
    return cursor

def build_kode(no):
    pre = 'dst'
    kode = str(no)
    while(len(kode) < 2):
        kode = '0' + kode
    return pre + kode

def cek_login(request, role):
    if 'email' in request.session:
        if request.session['role'] in role:
            return True
        else:
            return False
    else:
        return render(request, 'cannot.html')

# Create your views here.
def read_distribusi(request):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute("SELECT distribusi.*, v.nama FROM distribusi JOIN vaksin v on v.kode = distribusi.kode_vaksin;")
    dis = namedtuplefetchall(cursor)

    #print(dis)    
    response = {'distribusi':dis}
    cursor.close()
    return render(request, 'readDistribusi.html', response)

def detail_distribusi(request, kode):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute(f"SELECT * FROM distribusi WHERE kode='{kode}';")
    dis = namedtuplefetchall(cursor)[0]
    cursor.execute(f"SELECT * FROM penjadwalan WHERE kode_distribusi='{kode}';")
    pjd = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM instansi WHERE kode = '" + pjd.kode_instansi + "';")
    ins = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM lokasi_vaksin WHERE kode = '" + pjd.kode_lokasi + "';")
    lok = namedtuplefetchall(cursor)[0]
    # print(dis)
    cursor.execute(f"SELECT * FROM vaksin WHERE kode='{dis.kode_vaksin}';")
    vaksin = namedtuplefetchall(cursor)[0]
    
    response = {'dis':dis, 'nama_instansi':ins.nama_instansi, 'tanggal_waktu':str(pjd.tanggal_waktu), 'kuota':pjd.jumlah, 'penerima':pjd.kategori_penerima, 'lokasi_vak':lok.nama,'status': pjd.status, 'vaksin':vaksin}
    if pjd.jumlah_nakes != None:
        response['jumlah_nakes'] = pjd.jumlah_nakes
    
    if pjd.email_admin != None:
        response['email_admin'] = pjd.email_admin
    
    response['tanggal'] = str(dis.tanggal)

    cursor.close()
    return render(request, 'detailDistribusi.html', response)

def create_distribusi(request, id, tgl):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute("SELECT * FROM penjadwalan WHERE kode_instansi = '" + id + "' AND tanggal_waktu = '" + tgl + "';")
    pjd = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM instansi WHERE kode = '" + pjd.kode_instansi + "';")
    ins = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM lokasi_vaksin WHERE kode = '" + pjd.kode_lokasi + "';")
    lok = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM vaksin;")
    vaksin = namedtuplefetchall(cursor)
    cek_tanggal = str(pjd.tanggal_waktu)[:10]

    cursor.execute("SELECT kode FROM distribusi;")
    dis = namedtuplefetchall(cursor)
    kode_dis = 0
    for i in dis:
        kode_dis = max(kode_dis, int(i.kode[3:]))
    
    kode_dis = build_kode(kode_dis+1)
    
  #  print(vaksin)
    response = {'kode_distribusi':kode_dis, 'nama_instansi':ins.nama_instansi, 'tanggal_waktu':str(pjd.tanggal_waktu), 'kuota':pjd.jumlah, 'penerima':pjd.kategori_penerima, 'lokasi_vak':lok.nama,'status': pjd.status, 'vaksin':vaksin, 'cek_tanggal':cek_tanggal}
    if pjd.jumlah_nakes != None:
        response['jumlah_nakes'] = pjd.jumlah_nakes
    
    if pjd.email_admin != None:
        response['email_admin'] = pjd.email_admin
    
    if request.method == "POST":
        try:
            jumlah_vaksin = - int(request.POST['jumlah_vaksin'])
            tgl_now = str(datetime.datetime.now())[:19]
            # print(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {jumlah_vaksin},'{request.POST['kode_vaksin']}');")
            cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl_now}', {jumlah_vaksin},'{request.POST['kode_vaksin']}');")
            cursor.execute(f"INSERT INTO DISTRIBUSI VALUES ('{request.POST['kode']}','{request.POST['tanggal']}',{request.POST['biaya']},{request.POST['jumlah_vaksin']},'{request.POST['kode_vaksin']}');")
            cursor.execute(f"UPDATE penjadwalan SET kode_distribusi = '{request.POST['kode']}' WHERE kode_instansi = '{id}' AND tanggal_waktu = '{tgl}';")

            cursor.close()
            return HttpResponseRedirect('../../../')
        except Exception as e:
            print(e)
            response['message'] = 'Jumlah vaksin melebihi stok vaksin'

    cursor.close()
    return render(request, 'createDistribusi.html', response)

def update_distribusi(request, kode):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute(f"SELECT * FROM distribusi WHERE kode='{kode}';")
    dis = namedtuplefetchall(cursor)[0]
    cursor.execute(f"SELECT * FROM penjadwalan WHERE kode_distribusi='{kode}';")
    pjd = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM instansi WHERE kode = '" + pjd.kode_instansi + "';")
    ins = namedtuplefetchall(cursor)[0]
    cursor.execute("SELECT * FROM lokasi_vaksin WHERE kode = '" + pjd.kode_lokasi + "';")
    lok = namedtuplefetchall(cursor)[0]

    cursor.execute("SELECT * FROM vaksin;")
    vaksin = namedtuplefetchall(cursor)
    cek_tanggal = str(pjd.tanggal_waktu)[:10]

    vaksin_new = []
    for i in vaksin:
        sem = {'kode':i.kode, 'nama':i.nama}
        if i.kode == dis.kode_vaksin:
            sem['select'] = True
        vaksin_new.append(sem)


    response = {'dis':dis, 'nama_instansi':ins.nama_instansi, 'tanggal_waktu':str(pjd.tanggal_waktu), 'kuota':pjd.jumlah, 'penerima':pjd.kategori_penerima, 'lokasi_vak':lok.nama,'status': pjd.status, 'vaksin':vaksin_new, 'cek_tanggal':cek_tanggal}
    if pjd.jumlah_nakes != None:
        response['jumlah_nakes'] = pjd.jumlah_nakes
    
    if pjd.email_admin != None:
        response['email_admin'] = pjd.email_admin

    response['tanggal'] = str(dis.tanggal)

    if request.method == "POST":
        try:
            tgl = str(datetime.datetime.now())[:19]
            # print(tgl)

            if dis.kode_vaksin == request.POST['kode_vaksin']:
                jumlah_vaksin =  int(dis.jumlah_vaksin) - int(request.POST['jumlah_vaksin'])
                cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {jumlah_vaksin},'{request.POST['kode_vaksin']}');")
            else:
                jumlah_vaksin =  -int(request.POST['jumlah_vaksin'])
                cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {jumlah_vaksin},'{request.POST['kode_vaksin']}');")
                tgl = str(datetime.datetime.now()+datetime.timedelta(0,1))[:19]
                # print(tgl)
                jumlah_vaksin =  int(dis.jumlah_vaksin)
                cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {jumlah_vaksin},'{dis.kode_vaksin}');")
                
            # print(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {jumlah_vaksin},'{request.POST['kode_vaksin']}');")
            
            # cursor.execute(f"INSERT INTO DISTRIBUSI VALUES ('{request.POST['kode']}','{request.POST['tanggal']}',{request.POST['biaya']},{request.POST['jumlah_vaksin']},'{request.POST['kode_vaksin']}');")
            cursor.execute(f"UPDATE distribusi SET tanggal = '{request.POST['tanggal']}', biaya = {request.POST['biaya']}, jumlah_vaksin = {request.POST['jumlah_vaksin']}, kode_vaksin = '{request.POST['kode_vaksin']}' WHERE kode = '{request.POST['kode']}';")
            # cursor.execute(f"UPDATE penjadwalan SET kode_distribusi = '{request.POST['kode']}' WHERE kode_instansi = '{id}' AND tanggal_waktu = '{tgl}';")

            cursor.close()
            return HttpResponseRedirect('../../')
        except Exception as e:
            print(e)
            response['message'] = 'Jumlah vaksin melebihi stok vaksin'
 
    cursor.close()
    return render(request, "updateDistribusi.html", response)

def delete_distribusi(request, kode):
    if (not cek_login(request, ["admin_satgas"])):
        return render(request, 'cannot.html')

    cursor = new_cursor()

    cursor.execute(f"SELECT * FROM distribusi WHERE kode='{kode}';")
    dis = namedtuplefetchall(cursor)[0]

    today = datetime.date.today()
    tgl = str(datetime.datetime.now())[:19]

    if dis.tanggal <= today:
        cursor.close()
        return render(request, "cannotDelete.html")
    else:
        try:
            # print(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {dis.jumlah_vaksin}, '{dis.kode_vaksin}');")
            cursor.execute(f"INSERT INTO UPDATE_STOK VALUES('{request.session['email']}','{tgl}', {dis.jumlah_vaksin}, '{dis.kode_vaksin}');")
        except Exception as e:
            print(e)
        cursor.execute(f"DELETE FROM distribusi where kode='{kode}';")
        cursor.close()
        return HttpResponseRedirect('../../')
    

    