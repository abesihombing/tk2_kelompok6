from django.urls import path
from . import views

app_name = 'distribusi'

urlpatterns = [
    path('', views.read_distribusi, name='r-distribusi'),
    path('create/<str:id>/<str:tgl>/', views.create_distribusi, name='c-distribusi'),
    path('update/<str:kode>/', views.update_distribusi, name='u-distribusi'),
    path('detail/<str:kode>/', views.detail_distribusi, name='det-distribusi'),
    path('delete/<str:kode>/', views.delete_distribusi, name='del-distribusi'),
]
